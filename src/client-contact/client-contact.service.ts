import { Injectable, ForbiddenException } from '@nestjs/common';
import { CreateClientContactDto } from './dto/create-client-contact.dto';
import { UpdateClientContactDto } from './dto/update-client-contact.dto';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class ClientContactService {
  constructor(private prisma: PrismaService) {}
  async create(dto: CreateClientContactDto) {
    const clientContact = await this.prisma.client_contact.create({
      data: {
        ...dto,
      },
    });

    return clientContact;
  }

  findAll() {
    const clientsContact = this.prisma.client_contact.findMany();
    return clientsContact;
  }

  findOne(clientContactId: number) {
    const clientContact = this.prisma.client_contact.findFirst({
      where: { id: clientContactId },
    });
    if (!clientContact) return clientContact;
    return clientContact;
  }

  async update(clientContactId: number, dto: UpdateClientContactDto) {
    const clientContact = await this.prisma.client_contact.findUnique({
      where: {
        id: clientContactId,
      },
    });

    // check if user owns the bookmark
    if (!clientContact || clientContact.id !== clientContactId)
      throw new ForbiddenException('Access to resources denied');

    return this.prisma.client_contact.update({
      where: {
        id: clientContactId,
      },
      data: {
        ...dto,
      },
    });
  }

  async remove(clientContactId: number) {
    const clientContact = await this.prisma.client_contact.findUnique({
      where: {
        id: clientContactId,
      },
    });

    if (!clientContact || clientContact.id !== clientContactId)
      throw new ForbiddenException('Access to resources denied');

    await this.prisma.client_contact.delete({
      where: {
        id: clientContactId,
      },
    });
  }
}
