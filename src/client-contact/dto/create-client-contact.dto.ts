import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class CreateClientContactDto {
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  email: string;
  @IsString()
  @IsNotEmpty()
  name: string;
  @IsString()
  @IsNotEmpty()
  inquiry: string;
  @IsString()
  @IsNotEmpty()
  company: string;
}
