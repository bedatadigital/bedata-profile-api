import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ClientContactService } from './client-contact.service';
import { CreateClientContactDto } from './dto/create-client-contact.dto';
import { UpdateClientContactDto } from './dto/update-client-contact.dto';
import { JwtGuard } from 'src/auth/guard';
// import { GetUser } from 'src/auth/decorator';

@Controller('client-contact')
export class ClientContactController {
  constructor(private readonly clientContactService: ClientContactService) {}

  @Post()
  create(@Body() createClientContactDto: CreateClientContactDto) {
    return this.clientContactService.create(createClientContactDto);
  }

  @UseGuards(JwtGuard)
  @Get()
  findAll() {
    return this.clientContactService.findAll();
  }
  @UseGuards(JwtGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.clientContactService.findOne(+id);
  }
  @UseGuards(JwtGuard)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateClientContactDto: UpdateClientContactDto,
  ) {
    return this.clientContactService.update(+id, updateClientContactDto);
  }
  @UseGuards(JwtGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clientContactService.remove(+id);
  }
}
