import { ForbiddenException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthDto } from './dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private config: ConfigService,
    private jwt: JwtService,
  ) {}
  async signin(dto: AuthDto) {
    const userMatches =
      this.config.get('WEB_USERNAME') === dto.username &&
      this.config.get('WEB_PASSWORD') === dto.password;

    if (!userMatches) throw new ForbiddenException('Credentials incorrect');
    return this.signToken(dto.username);
  }
  async signToken(username: string): Promise<{ access_token: string }> {
    const payload = {
      username,
    };
    const secret = this.config.get('JWT_SECRET');
    const token = await this.jwt.signAsync(payload, {
      expiresIn: '15m',
      secret: secret,
    });
    return {
      access_token: token,
    };
  }
}
